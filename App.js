import React, {Component} from 'react';
import {StatusBar, View, StyleSheet, Platform} from 'react-native';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
// import AppReducer from './src/redux/reducers';
import {AppAuthNavigator} from './src/navigations/AppNavigation';
import configureStore from './src/redux/store';

const store = configureStore();

console.disableYellowBox = true;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingComplete: false,
      navigator: 'Auth', // 'Main' || 'Auth'
    };
  }

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <Provider store={store}>
          <AppAuthNavigator />
        </Provider>
      </View>
    );
  }
}

AppRegistry.registerComponent('StarterApp', () => App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
/* eslint-disable */
