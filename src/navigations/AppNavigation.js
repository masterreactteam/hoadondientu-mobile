import React from 'react';
import {connect} from 'react-redux';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
// import {Root} from 'native-base';
import {
  createReactNavigationReduxMiddleware,
  reduxifyNavigator,
} from 'react-navigation-redux-helpers';

import {AuthStack} from './AuthStackNavigation';
import {TabNavigator} from './BottomStackNavigaiton';

import SplashScreen from '../screens/SplashScreen';
import data from '../data/data.json';
// import {createMaterialTopTabNavigator} from 'react-navigation';
const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav,
);

const LoginAppNavigatorBegin = createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Splash: {
      screen: props => <SplashScreen {...props} data={data.main.splash} />,
    },
    AuthStack: {screen: AuthStack},
    HomeStack: {screen: TabNavigator},
  }),
);

const AppWithNavigationLoginBeginState = reduxifyNavigator(
  LoginAppNavigatorBegin,
  'root',
);

const mapStateToProps = state => ({
  state: state.nav,
});

// const AppWithNavigationState = reduxifyNavigator(RootNavigator, 'root');

// const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

const AppAuthNavigator = connect(mapStateToProps)(
  AppWithNavigationLoginBeginState,
);

export {AppAuthNavigator, LoginAppNavigatorBegin, middleware};
