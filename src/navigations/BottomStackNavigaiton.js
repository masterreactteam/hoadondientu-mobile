import React from 'react';
import {StyleSheet} from 'react-native';

import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {HomeStack} from './MainStackNavigation';
import Styles from '../common/styles';
import {Icon} from 'native-base';

const css = StyleSheet.create({
  icon: {
    // marginBottom: 5,
  },
});

export const TabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarColor: 'green',
        tabBarIcon: ({tintColor, focused}) => (
          <Icon
            name="home"
            type="MaterialCommunityIcons"
            style={[
              css.icon,
              // eslint-disable-next-line react-native/no-inline-styles
              {color: focused ? tintColor : 'gray', ...Styles.s22},
            ]}
          />
        ),
      },
    },
    Home1: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Gói khám',
        tabBarColor: 'green',
        tabBarIcon: ({tintColor, focused}) => (
          <Icon
            name="package"
            type="MaterialCommunityIcons"
            style={[
              css.icon,
              // eslint-disable-next-line react-native/no-inline-styles
              {color: focused ? tintColor : 'gray', ...Styles.s22},
            ]}
          />
        ),
      },
    },
    Home2: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Lịch khám',
        tabBarColor: 'green',
        tabBarIcon: ({tintColor, focused}) => (
          <Icon
            name="calendar-clock"
            type="MaterialCommunityIcons"
            style={[
              css.icon,
              // eslint-disable-next-line react-native/no-inline-styles
              {color: focused ? tintColor : 'gray', ...Styles.s22},
            ]}
          />
        ),
      },
    },
    Home3: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Hồ sơ',
        tabBarColor: 'green',
        tabBarIcon: ({tintColor, focused}) => (
          <Icon
            name="file-document"
            type="MaterialCommunityIcons"
            style={[
              css.icon,
              // eslint-disable-next-line react-native/no-inline-styles
              {color: focused ? tintColor : 'gray', ...Styles.s22},
            ]}
          />
        ),
      },
    },
    Home4: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Tư vấn',
        tabBarColor: 'green',
        tabBarIcon: ({tintColor, focused}) => (
          <Icon
            name="contacts"
            type="MaterialCommunityIcons"
            style={[
              css.icon,
              // eslint-disable-next-line react-native/no-inline-styles
              {color: focused ? tintColor : 'gray', ...Styles.s22},
            ]}
          />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    activeColor: '#3BAD87',
    inactiveColor: 'gray',
    shifting: false,
    barStyle: {
      height: 80,
      backgroundColor: '#FFFFFF',
      evalation: 0,
      padding: 0,
    },
  },
);
