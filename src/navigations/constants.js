import {StyleSheet} from 'react-native';
import {AppStyles} from '../AppStyles';
export const NAVIGATION = {
  WELCOME: 'Welcome',
  LOGIN: 'Login',
  LOGOUT: 'Logout',
  SIGNUP: 'SignUp',
  SPLASH: 'Splash',
  FORGOT_PASSWORD: 'ForgotPassword',
  HOME_STACK: 'HomeStack',
  AUTH_STACK: 'AuthStack',
  HOME: 'Home',
};

export const styles = StyleSheet.create({
  headerTitleStyle: {
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center',
    color: 'black',
    flex: 1,
    fontFamily: AppStyles.fontName.main,
  },
});
