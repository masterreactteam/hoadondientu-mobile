import React from 'react';
import {Animated, Easing, StyleSheet} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
// import {createBottomTabNavigator} from 'react-navigation-tabs';
import {fromLeft, fromRight} from 'react-navigation-transitions';
import HomeScreen from '../screens/HomeScreen';

import data from '../data/data.json';
import {styles, NAVIGATION} from './constants';

export const HomeStack = createStackNavigator(
  {
    Home: {
      screen: props => <HomeScreen {...props} data={data.main.signIn} />,
    },
  },
  {
    initialRouteName: NAVIGATION.HOME,
    transitionConfig: () => fromLeft(2000),
    headerMode: 'none',

    headerLayoutPreset: 'center',
    navigationOptions: ({navigation}) => ({
      headerTintColor: 'red',
      headerTitleStyle: styles.headerTitleStyle,
    }),
    cardStyle: {backgroundColor: '#FFFFFF'},
  },
);
