import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import Styles from '../../common/styles';
import colors from '../../common/colors';

const Loader = ({background, light, dark}) => {
  let backgroundColor = background
    ? light
      ? 'rgba(255,255,255,0.5)'
      : 'rgba(0,0,0,0.2)'
    : 'rgba(0,0,0,0)';

  return (
    <View
      style={[
        css.container,
        {backgroundColor: backgroundColor},
        Styles.Common.ColumnCenter,
      ]}>
      <View style={[css.mainLoading, {backgroundColor: 'transparent'}]}>
        <ActivityIndicator color={colors.submit} size={'large'} />
      </View>
    </View>
  );
};

const css = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    zIndex: 100,
  },
  mainLoading: {
    backgroundColor: '#000',
    padding: Styles.padding.small,
    borderRadius: 35,
  },
});

export default Loader;
