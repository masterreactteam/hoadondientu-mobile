import {Platform} from 'react-native';
import Toast from 'react-native-tiny-toast';
import normalize from 'react-native-normalize';

export const showCenter = (text, keyboardHeight = 0) => {
  return Toast.show(text, {
    containerStyle: {
      borderRadius: 30,
      // top: -(keyboardHeight),
      top: Platform.select({
        ios: -normalize(keyboardHeight),
        android: 0,
        default: 0,
      }),
    },
    animationDuration: 1000,
  });
};

export const showBottom = text => {
  return Toast.show(text, {
    containerStyle: {
      borderRadius: 30,
    },
    animationDuration: 1000,
  });
};
export default {showCenter, showBottom};
