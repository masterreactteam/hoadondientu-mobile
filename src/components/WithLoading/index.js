import React from 'react';
import {View} from 'react-native';
import Loader from '../Loader';

const WithLoading = Component => {
  return function WihLoadingComponent({isLoading, ...props}) {
    return (
      <View style={{width: '100%', height: '100%'}}>
        <Component {...props} />
        {isLoading && <Loader background light />}
      </View>
    );
  };
};
export default WithLoading;
