import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import { WebView } from 'react-native-webview';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import {AppIcon, AppStyles} from '../AppStyles';
import {Configuration} from '../Configuration';
import {UNAUTHENTICATED} from '../redux/constants';
import {execLogout} from '../redux/api/user';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      menuIcon: this.props.user && this.props.user.profileURL,
    });
  }

  render() {
    // console.log('this.props', this.props);
    return (
      <View style={styles.container}>
      <WebView
        source={{uri: 'https://goldenhealthcarevn.com/thu-vien-y-hoc/tin-tuc-y-hoc/'}}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Configuration.home.listing_item.offset,
  },
  title: {
    // fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
    color: AppStyles.color.title,
    fontSize: 25,
  },
  userPhoto: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginLeft: 5,
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
});
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(execLogout()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
