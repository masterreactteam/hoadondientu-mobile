// import library
import React, {Component} from 'react';
import {View, Animated, TouchableOpacity, Text} from 'react-native';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import normalize from 'react-native-normalize';

import {ButtonGradient} from '../../components/CoreUI/GradientButton';

// import css
import {css} from './css';

// import shared
import {VIEW_SIGNUP_REF, BTN_LOGIN_REF, FAST_IMAGE_REF} from './constants';

// import common
import Styles from '../../common/styles';
import images from '../../common/images';
import {NAVIGATION} from '../../navigations/constants';
import {
  getTokenAsync,
  clearTokenAsync,
} from '../../shared/asyncStorage/TokenStorageUtil';
import {width, height} from '../../common/styles';
import {AUTHENTICATED} from '../../redux/constants';
import {getUserAsync} from '../../shared/asyncStorage/UserStorageUtil';

const AnimatedFastImage = Animatable.createAnimatableComponent(FastImage);
const AnimatedButton = Animatable.createAnimatableComponent(TouchableOpacity);

export default class SpashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocus: false,
    };
    // setup animation
    this.logoHeightAnimation = new Animated.ValueXY();
    // this.logoHeightAnimation = new Animated.Value(height / 2 - height / 10);
    // this.springValue = new Animated.Value(height / 2 - height / 10);
  }

  componentDidMount = () => {
    this.mounted = true;
    this.navigationLogin();
  };

  componentWillUnmount = () => {
    this.mounted = false;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };

  createRef = nameRef => c => {
    this[nameRef] = c;
  };

  navigationLogin = async () => {
    const {navigation} = this.props;
    const token = await getTokenAsync();
    if (token !== null) {
      this.timeout = setTimeout(async () => {
        if (this.mounted) {
          const navigator = 'Main';
          this.setState({navigator, isLoadingComplete: true});
          const user = await getUserAsync();

          navigation.dispatch({
            type: AUTHENTICATED,
            user: JSON.parse(user),
          });
        }
        this.timeout = null;
      }, 1000);
    } else {
      this.handleLogoAnimation();
    }
  };

  /**
   * handle when onfus called
   */
  handleLogoAnimation = () => {
    this.timeout = setTimeout(async () => {
      this.timeout = null;
      this[FAST_IMAGE_REF].transitionTo({top: -200});
      this[VIEW_SIGNUP_REF].transitionTo({right: 0});
      this[BTN_LOGIN_REF].transitionTo({left: (0.3 * width) / 2});
    }, 2000);
  };

  /**
   * handle goto login
   * @param {string} type nameType router
   */
  handleNavigator = type => () => this.props.navigation.dispatch({type});

  render() {
    const {data} = this.props;
    return (
      <View style={[css.container, Styles.Common.ColumnCenter]}>
        <AnimatedFastImage
          ref={this.createRef(FAST_IMAGE_REF)}
          style={[css.bacground]}
          source={images.splashBackground}
          resizeMode={FastImage.resizeMode.contain}
        />
        <Animatable.View
          ref={this.createRef(VIEW_SIGNUP_REF)}
          style={[css['view-signup'], Styles.Common.RowCenter]}>
          <ButtonGradient
            style={[css['form-button-signup']]}
            onPress={this.handleNavigator(NAVIGATION.WELCOME)}
            height={normalize(55)}>
            <Text style={css['text-signup']}>{data.signup}</Text>
          </ButtonGradient>
        </Animatable.View>
        <AnimatedButton
          ref={this.createRef(BTN_LOGIN_REF)}
          onPress={this.handleNavigator(NAVIGATION.WELCOME)}
          style={[Styles.Common.ColumnCenter, css['form-button-login']]}>
          <Text style={css['text-signin']}>{data.signin}</Text>
        </AnimatedButton>
      </View>
    );
  }
}

SpashScreen.propTypes = {
  data: PropTypes.string,
};
