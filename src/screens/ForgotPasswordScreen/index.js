/**
 * NAME: ForgotPassword
 * FEATUTE: Handle feature when user forgot password
 */

// import library
import React from 'react';
import ReactNative, {
  View,
  Text,
  KeyboardAvoidingView,
  Animated,
  Easing,
} from 'react-native';
import {Container, Item, Input, Icon} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
import normalize from 'react-native-normalize';

// import component
import {Background as BackgroundForgotPassword} from '../../components/BackgroundImage';
import {ViewGradient} from '../../components/CoreUI/GradientButton';
import {DismissKeyboardHOC} from '../../components/DismissKeyboardHOC';

import {css} from './css';
import Images from '../../common/images';
import Styles from '../../common/styles';

import {gradientProps, inputProps, getNumberOfset} from './constants';
import {isSmallDevice, isAndroid} from '../../shared/getDeviceInfos';

const DismissKeyboardAvoidingView = DismissKeyboardHOC(KeyboardAvoidingView);
const AnimatedItem = Animatable.createAnimatableComponent(Item);
class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOffset: 0,
      isFocus: false,
    };

    // setup animation
    this.emailInputAnimation = new Animated.ValueXY();
    this.paddingAnimation = new Animated.Value(normalize(100));
  }

  /**
   * handle focus when keyboard open or close
   * @param {boolean} isFocus check focus of input
   */
  handleOnFocus = isFocus => () => {
    this.setState({numberOffset: getNumberOfset(isFocus), isFocus});

    // handle animation
    this.handleInputAnimation(isFocus);
  };

  /**
   * handle when onfus called
   * @param {boolean} isFocus check focus of input
   */
  handleInputAnimation = isFocus => {
    if (isFocus !== this.state.isFocus && isSmallDevice) {
      Animated.parallel([
        Animated.decay(this.emailInputAnimation, {
          velocity: {x: 0, y: isFocus ? -1 : 1},
          deceleration: 0.98,
        }),
        Animated.timing(this.paddingAnimation, {
          toValue: isFocus ? normalize(70) : normalize(100),
          duration: 50,
          // easing: Easing.linear
        }),
      ]).start();
    }
  };

  render() {
    const {data} = this.props;
    const {numberOffset, isFocus} = this.state;

    return (
      <Container style={Styles.Common.ColumnCenter}>
        <BackgroundForgotPassword
          data={Images.forgotPasswordBackground}
          style={css.bacground}
        />
        <View style={css.body}>
          <ViewGradient {...gradientProps} style={css.gradient}>
            <DismissKeyboardAvoidingView
              onPress={this.handleOnFocus(false)}
              keyboardVerticalOffset={numberOffset}
              behavior="padding"
              enabled
              style={[{flex: 1}]}>
              <View style={css.form}>
                {/* ----title---- */}

                <View style={css.title}>
                  <Animatable.Text
                    style={[
                      Styles.s20,
                      css['title-text'],
                      {paddingVertical: this.paddingAnimation},
                    ]}>
                    {data && data.title}
                  </Animatable.Text>
                  <AnimatedItem
                    rounded
                    style={[
                      this.emailInputAnimation.getLayout(isFocus),
                      css['form-item'],
                    ]}>
                    <Icon
                      type="AntDesign"
                      name="mail"
                      style={css['form-item-icon']}
                    />
                    <Input
                      {...inputProps(data.email)}
                      style={css['form-item-input']}
                      onSubmitEditing={this.handleOnFocus(false)}
                      onFocus={this.handleOnFocus(true)}
                    />
                  </AnimatedItem>
                </View>
                {/* ----end title---- */}

                {/* ----action---- */}
                <TouchableOpacity
                  style={[Styles.Common.ColumnCenter, css['form-button-send']]}>
                  <Text style={css['text-send']}>{data.send}</Text>
                </TouchableOpacity>
                {/* ----end action---- */}
              </View>
            </DismissKeyboardAvoidingView>
          </ViewGradient>
        </View>
      </Container>
    );
  }
}

export default ForgotPassword;

// proptype
ForgotPassword.propTypes = {
  data: PropTypes.object,
};
