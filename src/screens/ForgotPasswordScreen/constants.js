import normalize from 'react-native-normalize';
import {getTypeMobileDevice} from '../../shared/getDeviceInfos';
import {Platform} from 'react-native';
// iphone 8:(WIDTH, HEIGHT): (375,667)
// iphone 11:(WIDTH, HEIGHT): (414,896)

// gradient props
export const gradientProps = {
  colors: ['#21212199', '#21212199', '#010000', '#000000'],
  start: {x: 0, y: 0.3},
  end: {x: 0, y: 1},
};

// input props
export const inputProps = text => ({
  selectionColor: '#FFF',
  placeholderTextColor: '#FFF',
  placeholder: text,
});

export const getNumberOfset = isFocus => {
  const numberOfset = type =>
    ({
      small: normalize(200),
      medium: normalize(110),
      lagre: normalize(-30),
    }[type] || 0);
  console.log('numberOfset', numberOfset(getTypeMobileDevice()));
  return Platform.select({
    ios: isFocus ? numberOfset(getTypeMobileDevice()) : 0,
    android: isFocus ? numberOfset(getTypeMobileDevice()) : 0,
    default: 0,
  });
};
