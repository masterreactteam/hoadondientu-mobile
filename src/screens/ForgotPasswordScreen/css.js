import {StyleSheet, Dimensions, Platform} from 'react-native';
import colors from '../../common/colors';
import Styles from '../../common/styles';
import normalize from 'react-native-normalize';

const {width, height} = Dimensions.get('window');

const background = Platform.select({
  ios: {width, height, aspectRatio: 1, left: -40},
  android: {position: 'absolute', top: -100, width, height},
  default: {width, height, aspectRatio: 1, left: -40},
});

export const css = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    // flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  bacground: {...background},
  gradient: {
    width: '100%',
    height: '100%',
    opacity: 1,
  },

  title: {
    // flex: 1,
    // backgroundColor: 'tomato',
  },
  'title-text': {
    color: colors.white,
    paddingVertical: normalize(100),
    textAlign: 'center',
    fontFamily: Styles.FontFamily.robotoMain,
  },
  'title-text-focus': {
    color: colors.white,
    paddingVertical: normalize(70),
    textAlign: 'center',
    fontFamily: Styles.FontFamily.robotoMain,
  },
  form: {
    flex: 1,
    paddingHorizontal: 2 * Styles.padding.large,
    paddingVertical: 2 * Styles.padding.large,
    justifyContent: 'space-between',
  },
  'form-item': {
    backgroundColor: '#FFFFFF22',
    borderRadius: Styles.radius.small,
    borderColor: '#FFFFFF22',
    padding: Styles.padding.tiny,
    marginBottom: Styles.margin.large,
  },
  'form-item-icon': {
    color: '#FFF',
  },
  'form-item-input': {
    fontSize: Styles.FontSize.big,
    color: '#FFF',
  },
  'form-button-send': {
    width: '100%',
    backgroundColor: colors.submit,
    padding: Styles.padding.large,
    marginTop: Styles.margin.small,
    borderRadius: Styles.radius.small,
  },
  'form-forgot': {
    alignItems: 'flex-end',
    width: '100%',
  },
  'text-forgot': {
    color: '#FFFF',
    fontSize: Styles.FontSize.large,
  },
  'text-create': {
    color: '#FFFF',
    textAlign: 'center',
    fontSize: Styles.FontSize.large,
    // textDecorationLine: 'underline',
  },
  'text-send': {
    color: '#FFFF',
    fontSize: Styles.FontSize.large,
  },
  decoration: {
    height: 0, // height is '0' so that the view will not occupy space
    marginHorizontal: Styles.margin.tiny,
    borderTopColor: '#FFF',
    borderTopWidth: 0.7, // 'Thickness' of the underline
    marginTop: Styles.margin.tiny,
  },
  'form-button-create': {
    alignSelf: 'center',
  },
});
