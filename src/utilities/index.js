/*
Description: Format
Params:
data (string/object): datetime value
displayLocalTime (boolean): show local time or not
*/
export const formatDateTimeRequest = (strDateTime, char = '/') => {
  if (strDateTime && !strDateTime.includes('00:00')) {
    const arrDate = strDateTime.split(char);
    return `${arrDate[2]}-${arrDate[1]}-${arrDate[0]}T00:00:00.000Z`;
  }
  return strDateTime;
};

export const getYear = (strDateTime, char = '/') =>
  strDateTime.includes('00:00')
    ? strDateTime.split('-')[0]
    : strDateTime.split(char)[2];
