import {NAVIGATION} from '../../navigations/constants';
import {LoginAppNavigatorBegin} from '../../navigations/AppNavigation';
import {NavigationActions} from 'react-navigation';
import {AUTHENTICATED, UNAUTHENTICATED, WELCOME} from '../constants';

const initialNavState = LoginAppNavigatorBegin.router.getStateForAction(
  NavigationActions.navigate(NAVIGATION.SPLASH),
);

const nav = (state = initialNavState, action) => {
  let nextState;
  switch (action.type) {
    case WELCOME:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        NavigationActions.navigate({routeName: NAVIGATION.LOGIN}),
        state,
      );
      break;
    case NAVIGATION.SIGNUP:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        NavigationActions.navigate({routeName: NAVIGATION.SIGNUP}),
        state,
      );
      break;
    case AUTHENTICATED:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        NavigationActions.navigate({routeName: NAVIGATION.HOME_STACK}),
        state,
      );
      break;
    case UNAUTHENTICATED:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        NavigationActions.navigate({routeName: NAVIGATION.AUTH_STACK}),
        state,
      );
      break;
    case NAVIGATION.FORGOT_PASSWORD:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        NavigationActions.navigate({routeName: NAVIGATION.FORGOT_PASSWORD}),
        state,
      );
      break;
    case NAVIGATION.LOGOUT:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        NavigationActions.navigate({routeName: NAVIGATION.AUTH_STACK}),
        state,
      );
      break;
    default:
      nextState = LoginAppNavigatorBegin.router.getStateForAction(
        action,
        state,
      );
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};
export default nav;
