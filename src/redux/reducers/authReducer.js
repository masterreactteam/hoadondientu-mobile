import AsyncStorage from '@react-native-community/async-storage';
// import {NAVIGATION} from '../../navigations/constants';
import {NavigationActions} from 'react-navigation';
import {
  AUTHENTICATED,
  LOG_OUT,
  UNAUTHENTICATED,
  QRCODED,
  QRCODED_ERROR,
  RESET_USER,
} from '../constants';
import Toast from '../../components/Toast';
import {INVALID_QR_CODE} from '../../shared/constants/Messages';

const initialAuthState = {
  authenticated: false,
  user: {},
  dataFromQrCode: false,
};

const auth = (state = initialAuthState, action) => {
  switch (action.type) {
    case RESET_USER:
      return {...state, authenticated: true, user: {}, dataFromQrCode: false};
    case AUTHENTICATED:
      return {...state, authenticated: true, user: action.user};
    case UNAUTHENTICATED:
      return {...state, authenticated: false, user: {}};
    case QRCODED:
      return {...state, user: action.payload.user, dataFromQrCode: true};
    case QRCODED_ERROR:
      Toast.showBottom(INVALID_QR_CODE);
      break;
    case LOG_OUT:
      AsyncStorage.removeItem('@loggedInUserID:id');
      AsyncStorage.removeItem('@loggedInUserID:key');
      AsyncStorage.removeItem('@loggedInUserID:password');
      NavigationActions.navigate({routeName: 'AuthStack'});
      return {...state, authenticated: false, user: {}};
    default:
      return state;
  }
};
export default auth;
