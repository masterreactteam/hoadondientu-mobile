import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';

// import services from './services';
// import {routerReducer as location} from '../../router';

import nav from './navReducer';
import auth from './authReducer';
import loading from './loadingReducer';

const AppReducer = combineReducers({
  nav,
  auth,
  loading,
  form,
  // services,
  // location,
});

export default AppReducer;
