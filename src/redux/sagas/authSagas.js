import _ from 'lodash';
import {put, call, takeEvery, select} from 'redux-saga/effects';
import {execAuthenticate} from '../api/api-auth';
import {
  execGetUserInfo,
  execRegister,
  execGetPatientByQrCode,
} from '../api/api-user';
import {saveUserInfo} from '../api/user';
import {
  FETCH_AUTHENTICATE,
  UNAUTHENTICATED,
  FETCH_UNAUTHENTICATE,
  FETCH_REGISTER,
  FETCH_QRCODE,
  WELCOME,
} from '../constants';

import {cacheTokenAsync} from '../../shared/asyncStorage/TokenStorageUtil';
import AsyncStorage from '@react-native-community/async-storage';
import {setLoading} from '../actions/LoadingAction';
import {
  execAuthenticateSuccess,
  execAuthenticateError,
  execRegisterError,
  execQRCodeError,
  execQRCodeSuccess,
  execResetUser,
} from '../actions/authActions';
import {
  INVALID_LOGIN,
  LOGIN_FAILED,
  USER_CREATED,
  ERROR_OCCURED,
  INVALID_QR_CODE,
} from '../../shared/constants/Messages';
import Toast from '../../components/Toast';
import ActivatePatientPostModel from '../../models/activatePatientPostModel';

/* ====================================
 * HANDLE LOGIN
 * ====================================*/
export function* handleAuthenticate({payload}) {
  try {
    yield put(setLoading('users', true)); // loading wait for api
    const user = yield yield call(execAuthenticate, payload);
    cacheTokenAsync(_.get(user, 'access_token', null));

    // login success
    const userInfo = yield yield call(execGetUserInfo, payload.user.username);
    yield call(saveUserInfo, userInfo);
    yield put(execAuthenticateSuccess(userInfo));
    yield put(setLoading('users', false));
  } catch (err) {
    yield put(setLoading('users', false));
    yield put(execAuthenticateError(err.status));
    err.status === 404
      ? Toast.showCenter(INVALID_LOGIN, payload.keyboardHeight)
      : Toast.showCenter(LOGIN_FAILED, payload.keyboardHeight);
  }
}

export function* watchHandleAuthentication() {
  yield takeEvery(FETCH_AUTHENTICATE, handleAuthenticate); // call only
}

/* ====================================
 * HANDLE LOGOUT
 * ====================================*/

export function* handleLogout() {
  yield AsyncStorage.clear();
  yield put({type: UNAUTHENTICATED});
}

export function* watchHandleLogout() {
  yield takeEvery(FETCH_UNAUTHENTICATE, handleLogout); // call only
}

/* ====================================
 * HANDLE REGISTER
 * ====================================*/
export function* handleRegister({payload}) {
  try {
    yield put(setLoading('users', true)); // loading wait for api
    const response = yield call(execRegister, payload);

    switch (response.status) {
      case 200:
      case 201:
        response.json.isSuccess
          ? Toast.showBottom(USER_CREATED)
          : Toast.showBottom(response.json.errorMessage);
        response.json.isSuccess && (yield put({type: WELCOME}));
        break;
      default:
        Toast.showBottom(ERROR_OCCURED);
        break;
    }
    yield put(setLoading('users', false)); // loading wait for api
  } catch (err) {
    console.log('err', err);
    yield put(setLoading('users', false));
    yield put(execRegisterError(err.status));
  }
}

export function* watchHandleRegister() {
  yield takeEvery(FETCH_REGISTER, handleRegister); // call only
}

/* ====================================
 * HANDLE QR CODE
 * ====================================*/
export function* handleQRCode({payload}) {
  try {
    yield put(setLoading('users', true)); // loading wait for api
    yield put(execResetUser());
    const data = yield call(execGetPatientByQrCode, payload);
    console.log('response', data);
    yield put(setLoading('users', false)); // loading wait for api

    if (data.status === 200) {
      const patient = new ActivatePatientPostModel(data.json);
      yield put(execQRCodeSuccess(patient));
      return;
    }
    Toast.showBottom(INVALID_QR_CODE);
  } catch (err) {
    console.log('err', err);
    yield put(setLoading('users', false));
    yield put(execQRCodeError(err.status));
  }
}

export function* watchHandleQRCode() {
  yield takeEvery(FETCH_QRCODE, handleQRCode); // call only
}
