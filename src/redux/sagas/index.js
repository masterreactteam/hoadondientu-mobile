import {all} from 'redux-saga/effects';

import {
  watchHandleAuthentication,
  watchHandleLogout,
  watchHandleRegister,
  watchHandleQRCode,
} from './authSagas';

export default function* rootSaga() {
  yield all([
    watchHandleAuthentication(),
    watchHandleLogout(),
    watchHandleRegister(),
    watchHandleQRCode(),
  ]);
}
