import {
  UNAUTHENTICATED,
  SET_USER_INFO,
  FETCH_UNAUTHENTICATE,
} from '../constants';
import {createAction} from 'redux-actions';
import AsyncStorage from '@react-native-community/async-storage';
import {NavigationActions} from 'react-navigation';
import {
  clearUserAsync,
  cacheUserAsync,
} from '../../shared/asyncStorage/UserStorageUtil';

// actions
const setUserInfo = createAction(SET_USER_INFO);
// Logout
export const execLogout = () => {
  return {type: FETCH_UNAUTHENTICATE};
};

export const execClearStorage = () => {
  return {type: UNAUTHENTICATED};
};

export const saveUserInfo = async user => {
  await clearUserAsync();
  await cacheUserAsync(JSON.stringify(user));
  return setUserInfo(user);
};
