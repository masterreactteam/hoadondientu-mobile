// import { createAction } from 'redux-actions';
import {
  AUTHENTICATED,
  UNAUTHENTICATED,
  AUTHENTICATION_ERROR,
  FETCH_AUTHENTICATE,
  FETCH_REGISTER,
  REGISTERED,
  REGISTERED_ERROR,
  FETCH_QRCODE,
  QRCODED,
  QRCODED_ERROR,
  RESET_USER,
} from '../constants';

/* ====================================
 * HANDLE LOGIN
 * ====================================*/

const execFetchAuthenticate = (user, keyboardHeight) => {
  return {type: FETCH_AUTHENTICATE, payload: {user, keyboardHeight}};
};

const execAuthenticateSuccess = user => {
  return {type: AUTHENTICATED, user};
};
const execAuthenticateError = error => {
  return {type: AUTHENTICATION_ERROR, error};
};

const execUnAuthenticate = data => {
  return {type: UNAUTHENTICATED, data};
};

/* ====================================
 * HANDLE REGISTER
 * ====================================*/

const execResetUser = () => {
  return {type: RESET_USER};
};

const execFetchRegister = (user, type, isForce) => {
  return {type: FETCH_REGISTER, payload: {user, type, isForce}};
};

const execRegisterSuccess = () => {
  return {type: REGISTERED};
};
const execRegisterError = error => {
  return {type: REGISTERED_ERROR, error};
};

/* ====================================
 * HANDLE SCAN QR CODE
 * ====================================*/

const execFetchQRCode = code => {
  return {type: FETCH_QRCODE, payload: {code}};
};

const execQRCodeSuccess = user => {
  return {type: QRCODED, payload: {user}};
};
const execQRCodeError = error => {
  return {type: QRCODED_ERROR, error};
};

export {
  execResetUser,
  execFetchAuthenticate,
  execAuthenticateSuccess,
  execUnAuthenticate,
  execAuthenticateError,
  execFetchRegister,
  execRegisterSuccess,
  execRegisterError,
  execFetchQRCode,
  execQRCodeSuccess,
  execQRCodeError,
};
