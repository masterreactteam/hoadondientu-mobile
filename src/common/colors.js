export default {
  tintGradient: {
    start: '#EF8D00',
    end: '#F2A64E',
  },
  white: '#FFFFFF',
  submit: '#F28D00',
  background: '',
  secondBackgound: '#31A366',
};
